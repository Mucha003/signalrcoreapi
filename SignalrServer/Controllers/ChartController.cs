﻿namespace SignalrServer.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.SignalR;
    using SignalrServer.Data;
    using SignalrServer.Hubs;
    using SignalrServer.TimerFeatures;
    using System.Threading.Tasks;

    [Route("api/[controller]")]
    [ApiController]
    public class ChartController : ControllerBase
    {
        private IHubContext<ChartHub> _hub;

        public ChartController(IHubContext<ChartHub> hub)
        {
            this._hub = hub;
        }

        public async Task<IActionResult> Get()
        {
           // providing a callback function as a parameter
           var timerManager = new TimerManager(() =>
            // This callback function will be executed every two seconds.
            this._hub.Clients.All.SendAsync("SendDataChartServer", 
                                            DataManager.GetData()));
            // On va envoyer a tous les client Abonnée a cette methode (SendDataChartServer)

            return Ok(new { Message = "Envoi OK" });
        }
    }
}