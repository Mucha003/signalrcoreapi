﻿namespace SignalrServer.TimerFeatures
{
    using System;
    using System.Threading;

    public class TimerManager
    {
        private Timer _timer;
        private AutoResetEvent _autoResetEvent;
        private Action _action;

        public DateTime TimerStarted { get; }

        public TimerManager(Action action)
        {
            _action = action;
            _autoResetEvent = new AutoResetEvent(false);
            // action pour exécuter la fonction de Callback 
            // transmise toutes les deux secondes
            // Le minuteur fera une pause d'une seconde avant la première exécution
            _timer = new Timer(Execute, _autoResetEvent, 1000, 2000);
            TimerStarted = DateTime.Now;
        }

        public void Execute(object stateInfo)
        {
            _action();
            // nous créons simplement un créneau de soixante secondes pour l'exécution,
            // afin d'éviter une boucle de minuterie illimitée.
            if ((DateTime.Now - TimerStarted).Seconds > 60)
            {
                _timer.Dispose();
            }
        }
    }
}
