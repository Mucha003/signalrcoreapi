﻿namespace SignalrServer.Hubs
{
    using Microsoft.AspNetCore.SignalR;
    using SignalrServer.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class ChartHub : Hub
    {
        // Cette méthode BroadcastChartData recevra le message du client,
        public async Task BroadcastChartData(List<ChartModel> data)
        {
            // broadcastchartdata diffusera à tous les clients écoutant l'événement.
            await Clients.All.SendAsync("broadcastchartdata", data);
        }
    }
}
